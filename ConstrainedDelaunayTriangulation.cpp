#include "ConstrainedDelaunayTriangulation.h"

CConstrainedDelaunayTriangulation::CConstrainedDelaunayTriangulation()
	: m_uPointCount( 0 )
{
	size_t uMemoryRequired( MPE_PolyMemoryRequired( ms_kuMaxPoints ) );
	void* pMemory = calloc( uMemoryRequired, 1 );
	MPE_PolyInitContext( &m_oPolyContext, pMemory, ms_kuMaxPoints );
}

CConstrainedDelaunayTriangulation::~CConstrainedDelaunayTriangulation()
{
	free( m_oPolyContext.Memory );
}

void CConstrainedDelaunayTriangulation::AddPoint( CF32Vector2 oPoint )
{
	if ( m_uPointCount >= ms_kuMaxPoints )
	{
		return;
	}
	m_vPoints.push_back( oPoint );
	MPEPolyPoint* pPoint = MPE_PolyPushPoint( &m_oPolyContext );
	pPoint->X = oPoint.X();
	pPoint->Y = oPoint.Y();
	++m_uPointCount;
}

void CConstrainedDelaunayTriangulation::CreatePolygon()
{
	if (m_oPolygon.size() > 0 || m_vPoints.size() < 3)
	{
		m_vPoints.clear();
		return;
	}
	for( CF32Vector2 oPoint : m_vPoints )
	{
		m_oPolygon.push_back( oPoint );
	}
	MPE_PolyAddEdge( &m_oPolyContext );
	m_vPoints.clear();
}

void CConstrainedDelaunayTriangulation::CreateHole()
{
	if (m_vPoints.size() < 3)
	{
		return;
	}
	CPolygon2 oPolygon;
	for( CF32Vector2 oPoint : m_vPoints )
	{
		oPolygon.push_back( oPoint );
	}
	m_vHoles.push_back( oPolygon );
	MPE_PolyAddHole( &m_oPolyContext );
	m_vPoints.clear();
}

void CConstrainedDelaunayTriangulation::Triangulate()
{
	MPE_PolyTriangulate( &m_oPolyContext );
	for( u32 uTriangleIndex( 0 ); uTriangleIndex < m_oPolyContext.TriangleCount; ++uTriangleIndex )
	{
		MPEPolyTriangle* pTriangle( m_oPolyContext.Triangles[ uTriangleIndex ] );
		MPEPolyPoint* pPointA( pTriangle->Points[ 0 ] );
		MPEPolyPoint* pPointB( pTriangle->Points[ 1 ] );
		MPEPolyPoint* pPointC( pTriangle->Points[ 2 ] );
		CTriangle2 oTriangle;
		oTriangle[ 0 ].XY( pPointA->X, pPointA->Y );
		oTriangle[ 1 ].XY( pPointB->X, pPointB->Y );
		oTriangle[ 2 ].XY( pPointC->X, pPointC->Y );
		m_vTriangles.push_back( oTriangle );
	}
}

void CConstrainedDelaunayTriangulation::Reset()
{
	m_vPoints.clear();
	m_oPolygon.clear();
	m_vHoles.clear();
	m_vTriangles.clear();
	m_uPointCount = 0;
	memset( m_oPolyContext.Memory, 0, MPE_PolyMemoryRequired( ms_kuMaxPoints ) );
	MPE_PolyInitContext( &m_oPolyContext, m_oPolyContext.Memory, ms_kuMaxPoints );
}
