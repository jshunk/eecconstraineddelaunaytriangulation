#pragma once

#include <vector>
#include "Vector/Vector2.h"
#define MPE_POLY2TRI_IMPLEMENTATION
#define insternal_static extern
#pragma warning( push )
#pragma warning( disable : 4505 )
#pragma warning( disable : 4706 )
#include "Fast Poly2Tri/MPE_fastpoly2tri.h"
#pragma warning( pop )

class CConstrainedDelaunayTriangulation
{
private:
	static const u32	ms_kuMaxPoints = 10000;

public:
										CConstrainedDelaunayTriangulation();
										~CConstrainedDelaunayTriangulation();
	void								AddPoint( CF32Vector2 oPoint );
	void								CreatePolygon();
	void								CreateHole();
	void								Triangulate();
	const std::vector< CF32Vector2 >&	GetPoints() const { return m_vPoints; }
	const CPolygon2&					GetPolygon() const { return m_oPolygon; }
	const std::vector< CPolygon2 >&		GetHoles() const { return m_vHoles; }
	const std::vector< CTriangle2 >&	GetTriangles() const { return m_vTriangles; }
	void								Reset();

private:
	std::vector< CF32Vector2 >	m_vPoints;
	CPolygon2					m_oPolygon;
	std::vector< CPolygon2 >	m_vHoles;
	std::vector< CTriangle2 >	m_vTriangles;
	MPEPolyContext				m_oPolyContext;
	u32							m_uPointCount;
};
